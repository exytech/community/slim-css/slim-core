<p align="center">
    <a>
        <img src="./logo.svg" alt="Slim-CSS logo" width="200" height="165" />
    </a>
</p>

<h1 align="center"> Slim CSS </h1>

<p align="center">
  Beautiful and SlimCSS Framework based on Flexbox and Grid
  <br>
  <a href="./examples/index.html"><strong>Explore Slim-CSS docs »</strong></a>
  <br>
   <br>
  <a href="https://gitlab.com/exytech/community/slim-css/slim-core/-/issues/new">Report bug</a>
  ·
  <a href="https://gitlab.com/exytech/community/slim-css/slim-core/-/issues/new">Request feature</a>
</p>

## Slim-CSS v1.0.0
Our default branch is for development of our Slim-CSS v1.0.0 release.


## Table of contents

- [Quick install](#quick-install)
- [Copyright and license](#copyright-and-license)


## Quick install

Several quick install options are available:


### Repo

```sh
git clone https://gitlab.com/exytech/community/slim-css/slim-core.git
```


### NPM

```sh
npm install @slim-css/core
```

**or**

### Yarn

```sh
yarn add @slim-css/core
```

### Import
After installation, you can import the CSS file into your project using this snippet:

```sh
@import '@slim-css/core/css/slim-core.css'
@import '@slim-css/core/css/slim-core-components.css'
```

**or**

```sh
@import '@slim-css/core/css/slim-core-full.css'
```

### CDN
https://cdn.jsdelivr.net/npm/@slim-css/core@1.0.0-alpha/

- slim-core compact file
  - [slim-core](https://cdn.jsdelivr.net/npm/@slim-css/core@1.0.0-alpha/css/slim-core.css)
  - [slim-core-components](https://cdn.jsdelivr.net/npm/@slim-css/core@1.0.0-alpha/css/slim-core-components.css)
  - [slim-core-full](https://cdn.jsdelivr.net/npm/@slim-css/core@1.0.0-alpha/css/slim-core-full.css)

- slim-core minified files
  - [slim-core](https://cdn.jsdelivr.net/npm/@slim-css/core@1.0.0-alpha/css/slim-core.min.css)
  - [slim-core-components](https://cdn.jsdelivr.net/npm/@slim-css/core@1.0.0-alpha/css/slim-core-components.min.css)
  - [slim-core-full](https://cdn.jsdelivr.net/npm/@slim-css/core@1.0.0-alpha/css/slim-core-full.min.css)


## Copyright and license
Code copyright 2021 SOCGNA KOUYEM Childéric. Code released under [the MIT license](https://gitlab.com/exytech/community/slim-css/slim-core/-/blob/develop/LICENSE).
